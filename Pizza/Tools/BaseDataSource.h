//
//  BaseDataSource.h
//  Pizza
//
//  Created by Nikita Titov on 27/02/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellItem.h"

typedef void (^ConfigureCellBlock)(id<CellItem> cell, id item);
typedef id (^ItemMappingBlock)(id item);
typedef NSString *(^CellIdentifierBlock)(id item);

@protocol BaseDataSourceDelegate;

@interface BaseDataSource : NSObject <UITableViewDataSource>

@property (weak, nonatomic) id<BaseDataSourceDelegate> delegate;

- (id)initWithCellIdentifier:(NSString *)cellIdentifier itemMappingBlock:(ItemMappingBlock)itemMappingBlock configureCellBlock:(ConfigureCellBlock)configureCellBlock;

- (id)initWithCellIdentifierBlock:(CellIdentifierBlock)cellIdentifierBlock itemMappingBlock:(ItemMappingBlock)itemMappingBlock configureCellBlock:(ConfigureCellBlock)configureCellBlock;

- (id)itemAtIndexPath:(NSIndexPath *)indexPath;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (BOOL)rowAtIndexPathIsLast:(NSIndexPath *)indexPath;

@end

@protocol BaseDataSourceDelegate <NSObject>

- (void)willShowCellForLastRow;

@end
