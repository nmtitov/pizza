//
//  FetchedResultsControllerDataSource.h
//  Pizza
//
//  Created by Nikita Titov on 27/02/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//


#import <CoreData/CoreData.h>
#import "BaseDataSource.h"

@interface FetchedResultsControllerDataSource : BaseDataSource

@property (strong, nonatomic, readonly) NSFetchedResultsController *fetchedResultsController;

- (instancetype)initWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController
                                  cellIdentifier:(NSString *)cellIdentifier
                                itemMappingBlock:(ItemMappingBlock)itemMappingBlock
                              configureCellBlock:(ConfigureCellBlock)configureCellBlock
                       latestItemSortDescriptors:(NSArray *)sortDescriptors;

- (instancetype)initWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController
                             cellIdentifierBlock:(CellIdentifierBlock)cellIdentifierBlock
                                itemMappingBlock:(ItemMappingBlock)itemMappingBlock
                              configureCellBlock:(ConfigureCellBlock)configureCellBlock
                       latestItemSortDescriptors:(NSArray *)sortDescriptors;

- (id)latestItem;
- (id)firstItem;
- (id)lastItem;
- (NSUInteger)numberOfItems;

@property (copy, nonatomic) ItemMappingBlock sectionTitleMapping;
@property (copy, nonatomic) ItemMappingBlock sectionIndexTitlesMapping;

@end
