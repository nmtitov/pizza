//
//  FetchedResultsControllerDataSource.m
//  Pizza
//
//  Created by Nikita Titov on 27/02/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "FetchedResultsControllerDataSource.h"

@interface FetchedResultsControllerDataSource ()

@property (strong, nonatomic) NSArray *sortDescriptors;

@end

@implementation FetchedResultsControllerDataSource

- (id)initWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController
                        cellIdentifier:(NSString *)cellIdentifier
                      itemMappingBlock:(ItemMappingBlock)itemMappingBlock
                    configureCellBlock:(ConfigureCellBlock)configureCellBlock
             latestItemSortDescriptors:(NSArray *)sortDescriptors {
    
    self = [super initWithCellIdentifier:cellIdentifier
                        itemMappingBlock:itemMappingBlock
                      configureCellBlock:configureCellBlock];

    if (!self) return nil;
    
    _fetchedResultsController = fetchedResultsController;
    self.sortDescriptors = sortDescriptors;
    
    return self;
}

- (instancetype)initWithFetchedResultsController:(NSFetchedResultsController *)fetchedResultsController
                             cellIdentifierBlock:(CellIdentifierBlock)cellIdentifierBlock
                                itemMappingBlock:(ItemMappingBlock)itemMappingBlock
                              configureCellBlock:(ConfigureCellBlock)configureCellBlock
                       latestItemSortDescriptors:(NSArray *)sortDescriptors {
    
    self = [super initWithCellIdentifierBlock:cellIdentifierBlock
                             itemMappingBlock:itemMappingBlock
                           configureCellBlock:configureCellBlock];
    
    if (!self) return nil;
    
    _fetchedResultsController = fetchedResultsController;
    self.sortDescriptors = sortDescriptors;
    
    return self;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (id)latestItem {
    return [[self.fetchedResultsController.sections.firstObject objects]
            sortedArrayUsingDescriptors:self.sortDescriptors].firstObject;
}

- (id)firstItem {
    return [[self.fetchedResultsController.sections.firstObject objects]
            sortedArrayUsingDescriptors:self.sortDescriptors].firstObject;
}

- (id)lastItem {
    return [[self.fetchedResultsController.sections.firstObject objects]
            sortedArrayUsingDescriptors:self.sortDescriptors].lastObject;
}

- (NSUInteger)numberOfItems {
    return [self tableView:nil numberOfRowsInSection:0];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.fetchedResultsController sections][(NSUInteger) section] numberOfObjects];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *name = [[self.fetchedResultsController sections][(NSUInteger) section] name];
    if (self.sectionTitleMapping) {
        return self.sectionTitleMapping(name);
    }
    return name;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    NSArray *titles = self.fetchedResultsController.sectionIndexTitles;
    if (self.sectionIndexTitlesMapping) {
        return self.sectionIndexTitlesMapping(titles);
    }
    return titles;
}

- (BOOL)rowAtIndexPathIsLast:(NSIndexPath *)indexPath {
    NSInteger numberOfRows = [self tableView:nil numberOfRowsInSection:0];
    if (numberOfRows == 0) {
        return YES;
    }
    return indexPath.row + 1 == numberOfRows;
}

@end
