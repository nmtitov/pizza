//
//  BaseDataSource.m
//  Pizza
//
//  Created by Nikita Titov on 27/02/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "BaseDataSource.h"

@interface BaseDataSource ()

@property (copy, nonatomic, readonly) NSString *cellIdentifier;
@property (copy, nonatomic, readonly) CellIdentifierBlock cellIdentifierBlock;
@property (copy, nonatomic, readonly) ConfigureCellBlock configureCellBlock;
@property (copy, nonatomic, readonly) ItemMappingBlock itemMappingBlock;

@end

@implementation BaseDataSource

- (id)initWithCellIdentifier:(NSString *)cellIdentifier
            itemMappingBlock:(ItemMappingBlock)itemMappingBlock
          configureCellBlock:(ConfigureCellBlock)configureCellBlock {
    
    NSParameterAssert(cellIdentifier);
    NSParameterAssert(configureCellBlock);

    self = [super init];

    if (!self) {
        return nil;
    }
    
    _cellIdentifier = [cellIdentifier copy];
    _configureCellBlock = [configureCellBlock copy];
    
    _itemMappingBlock = [itemMappingBlock copy];

    return self;
}

- (id)initWithCellIdentifierBlock:(CellIdentifierBlock)cellIdentifierBlock itemMappingBlock:(ItemMappingBlock)itemMappingBlock configureCellBlock:(ConfigureCellBlock)configureCellBlock {
    
    NSParameterAssert(cellIdentifierBlock);
    NSParameterAssert(configureCellBlock);
    
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _cellIdentifierBlock = [cellIdentifierBlock copy];
    _configureCellBlock = [configureCellBlock copy];
    
    _itemMappingBlock = [itemMappingBlock copy];
    
    return self;
}

- (id)mappedItemAtIndexPath:(NSIndexPath *)indexPath {
    id item = [self itemAtIndexPath:indexPath];
    if (!self.itemMappingBlock) {
        return item;
    }
    return self.itemMappingBlock(item);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id item = [self mappedItemAtIndexPath:indexPath];
    
    NSString *identifier;
    if (self.cellIdentifier) {
        identifier = self.cellIdentifier;
    }
    else if (self.cellIdentifierBlock) {
        identifier = self.cellIdentifierBlock(item);
    }
    NSAssert(identifier, @"Cell identifier is required!");
    
    id cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];

    self.configureCellBlock(cell, item);
    
    if ([self rowAtIndexPathIsLast:indexPath]) {
        [self.delegate willShowCellForLastRow];
    }
    
    return cell;
}

- (BOOL)rowAtIndexPathIsLast:(NSIndexPath *)indexPath {
    return NO;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    id item = [self mappedItemAtIndexPath:indexPath];
    id<CellItem> cellItem = (id<CellItem>)cell;
    self.configureCellBlock(cellItem, item);
}

#pragma mark - abstract

- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    [self doesNotRecognizeSelector:_cmd];
    return 0;
}

@end
