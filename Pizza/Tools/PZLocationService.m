//
//  PZLocationService.m
//  SeaBattle
//
//  Created by Nikita Titov on 13/05/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZLocationService.h"
#import "PZHelper.h"

@interface PZLocationService ()<CLLocationManagerDelegate>

@property (readwrite, strong, nonatomic) RACSignal *locations;

@property (copy, nonatomic) CLLocation *location;
@property (copy, nonatomic) CLHeading *heading;

@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation PZLocationService

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self.locationManager startUpdatingLocation];
    
    if ([self.locationManager respondsToSelector:NSSelectorFromString(@"requestWhenInUseAuthorization")]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    self.locations = [[[RACObserve(self, location) filter:[PZHelper exists]] logAll] replayLast];
    
    return self;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newLocation = locations.lastObject;
    self.location = newLocation;
}

@end
