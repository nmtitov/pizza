//
//  FetchedResultsControllerDataSource.h
//  Pizza
//
//  Created by Nikita Titov on 27/02/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@class FetchedResultsControllerDataSource;

@interface FetchedResultsTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) FetchedResultsControllerDataSource *dataSource;

- (void)performFetch;

@end
