//
//  CellItem.h
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CellItem <NSObject>

@property (strong, nonatomic) id item;

@end
