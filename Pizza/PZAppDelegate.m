//
//  PZAppDelegate.m
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZAppDelegate.h"
#import "PZPizzaPlacesDetailViewController.h"
#import "PZPizzaPlacesViewController.h"
#import "PZMasterViewModel.h"
#import <AFNetworkActivityLogger.h>
#import <UIColor-HexString/UIColor+HexString.h>
#import <MagicalRecord/MagicalRecord+Setup.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalRecord.h>

NSString * const kCLIENT_ID = @"C1GHTUXRAOZFF2MENRIOAST11TDETLWS3NNUVUJGHDTHXEEY";
NSString * const kCLIENT_SECRET = @"LDLT4I0GJSLV5M3K4C3ACVWWZRE1BYLYBGQBYL1U0KFOXQTP";
NSString * const kVERSION = @"20130815";
NSString * const kNOTIFICATION_SAVE = @"NOTIFICATION_SAVE";

static NSString * const kBaseURLString = @"https://api.foursquare.com/v2/";

PZAppDelegate * App();

@interface PZAppDelegate ()

@end

@implementation PZAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [MagicalRecord setupCoreDataStackWithInMemoryStore];
    self.moc = [NSManagedObjectContext MR_defaultContext];
    
    [UINavigationBar appearance].barTintColor = [UIColor colorWithHexString:@"e94339"];
    [UINavigationBar appearance].barStyle = UIBarStyleBlack;
    [UINavigationBar appearance].tintColor = [UIColor colorWithHexString:@"fefefe"];
    [UINavigationBar appearance].titleTextAttributes = @{
        NSForegroundColorAttributeName: [UIColor colorWithHexString:@"fefefe"],
    };
    
    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
    PZPizzaPlacesViewController *controller = (PZPizzaPlacesViewController *)navigationController.topViewController;
    controller.viewModel = [[PZMasterViewModel alloc] init];
    controller.moc = self.moc;
    
    self.http = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURLString]];
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    
    self.loc = [[PZLocationService alloc] init];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

@end

PZAppDelegate * App() {
    return [[UIApplication sharedApplication] delegate];
}
