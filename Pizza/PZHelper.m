//
//  PZHelper.m
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZHelper.h"
#import "PZAppDelegate.h"
#import "NSString+hasContent__NT.h"
#import <Mantle/Mantle.h>
#import <ReactiveCocoa.h>

@implementation PZHelper

+ (FilterBlock)exists {
    return ^BOOL(id x) {
        return x? YES: NO;
    };
}

+ (FilterBlock)hasContent {
    return ^BOOL(NSString *string) {
        return string.hasContent__NT;
    };
}

+ (TryMapBlock)collectionOfClass:(Class)klass {
    return ^id(RACTuple *response, NSError *__autoreleasing *errorPtr) {
        @try {
            NSDictionary *json = response.first;
            NSDictionary *response = json[@"response"];
            NSArray *venuesJSON = response[@"venues"];
            return [MTLJSONAdapter modelsOfClass:klass fromJSONArray:venuesJSON error:errorPtr];
        }
        @catch (NSException *exception) {
            if (errorPtr != NULL) {
                *errorPtr = [PZError parseErrorWithMessage:exception.reason];
            }
            return nil;
        }
    };
}

+ (TryMapBlock)itemOfClass:(Class)klass {
    return ^id(RACTuple *response, NSError *__autoreleasing *errorPtr) {
        @try {
            id json = response.first;
            return [MTLJSONAdapter modelOfClass:klass fromJSONDictionary:json error:errorPtr];
        }
        @catch (NSException *exception) {
            if (errorPtr != NULL) {
                *errorPtr = [PZError parseErrorWithMessage:exception.reason];
            }
            return nil;
        }
    };
}

@end
