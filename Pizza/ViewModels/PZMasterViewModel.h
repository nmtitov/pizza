//
//  PZMasterViewModel.h
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <ReactiveCocoa.h>
#import "PZPizzaPlace.h"

@interface PZMasterViewModel : NSObject

@property (strong, nonatomic) RACCommand *load;

@end
