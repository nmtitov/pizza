//
//  PZMasterViewModel.m
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZMasterViewModel.h"
#import "PZAppDelegate.h"

@implementation PZMasterViewModel

- (instancetype)init {
    self = [super init];
    if (!self) return nil;
    
    @weakify(self);
    self.load = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        return [self loadSignal];
    }];
    
    [self.load.executionSignals subscribeNext:^(id x) {
        [x subscribeNext:^(NSArray *items) {
            @strongify(self);
            [self removeOld];
            [self saveResult:items];
        }];
    }];
    
    return self;
}

- (void)removeOld {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:[PZPizzaPlace managedObjectEntityName]];
    fetchRequest.includesPropertyValues = NO;
    
    NSError *error;
    NSArray *fetchedObjects = [App().moc executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects) {
        [App().moc deleteObject:object];
    }
}

- (void)saveResult:(id)result {
    NSParameterAssert(result);
    
    NSArray *models = [result isKindOfClass:[NSArray class]] ? result : @[result];
    for (MTLModel<MTLManagedObjectSerializing> *model in models) {
        NSError *error = nil;
        [MTLManagedObjectAdapter managedObjectFromModel:model
                                   insertingIntoContext:App().moc
                                                  error:&error];
        NSAssert(error == nil, @"%@ saveResult failed with error: %@", self, error);
    }
    
    NSManagedObjectContext *context = App().moc;
    
    [context performBlockAndWait:^{
        if ([context hasChanges]) {
            NSError *error = nil;
            [context save:&error];
            NSAssert(error == nil, @"%@ saveResult failed with error: %@", self, error);
            [[NSNotificationCenter defaultCenter] postNotificationName:kNOTIFICATION_SAVE object:nil];
        }
    }];
}

- (RACSignal *)loadSignal {
    return [[App().loc.locations take:1] flattenMap:^RACStream *(CLLocation *location) {
        NSString *ll = [NSString stringWithFormat:@"%.02f,%.02f", location.coordinate.latitude, location.coordinate.longitude];
        NSDictionary *parameters = @{
            @"client_id": kCLIENT_ID,
            @"client_secret": kCLIENT_SECRET,
            @"v": kVERSION,
            @"ll": ll,
            @"query": @"pizza",
            @"limit": @5,
        };
        return [[[[App().http rac_GET:@"venues/search" parameters:parameters] logAll] tryMap:[PZHelper collectionOfClass:[PZPizzaPlace class]]] logAll];
    }];
}

@end
