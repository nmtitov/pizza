//
//  PZError.m
//  Nikita Titov
//
//  Created by Nikita Titov on 14/05/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZError.h"

@implementation PZError

+ (instancetype)requestError {
    return [self requestErrorWithMessage:nil];
}

+ (instancetype)requestErrorWithMessage:(NSString *)message {
    NSDictionary *userInfo = @{
       NSLocalizedDescriptionKey: message ?: @"Bad request",
    };
    return [self errorWithDomain:PZServerErrorDomain code:PZServerErrorRequestError userInfo:userInfo];
}

+ (instancetype)serverError {
    return [self serverErrorWithMessage:nil];
}

+ (instancetype)serverErrorWithMessage:(NSString *)message {
    NSDictionary *userInfo = @{
       NSLocalizedDescriptionKey: message ?: @"Server error",
    };
    return [self errorWithDomain:PZServerErrorDomain code:PZServerErrorServerError userInfo:userInfo];
}

+ (instancetype)parseError {
    return [self authenticationErrorWithMessage:nil];
}

+ (instancetype)parseErrorWithMessage:(NSString *)message {
    NSDictionary *userInfo = @{
       NSLocalizedDescriptionKey: message ?: @"Parse error",
    };
    return [self errorWithDomain:PZServerErrorDomain code:PZServerErrorParseError userInfo:userInfo];
}

+ (instancetype)authenticationError {
    return [self authenticationErrorWithMessage:nil];
}

+ (instancetype)authenticationErrorWithMessage:(NSString *)message {
    NSDictionary *userInfo = @{
       NSLocalizedDescriptionKey: message ?: @"Server error",
    };
    return [self errorWithDomain:PZServerErrorDomain code:PZServerErrorAuthenticationError userInfo:userInfo];
}

@end

NSString * const PZServerErrorDomain = @"PZServerErrorDomain";
NSInteger const PZServerErrorRequestError = 1;
NSInteger const PZServerErrorServerError = 2;
NSInteger const PZServerErrorParseError = 3;
NSInteger const PZServerErrorAuthenticationError = 4;
