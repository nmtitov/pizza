//
//  PZHelper.h
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id (^MapBlock)(id x);
typedef BOOL (^FilterBlock)(id x);
typedef id (^TryMapBlock)(id json, NSError *__autoreleasing *errorPtr);

@interface PZHelper : NSObject

+ (FilterBlock)exists;
+ (FilterBlock)hasContent;

+ (TryMapBlock)collectionOfClass:(Class)klass;
+ (TryMapBlock)itemOfClass:(Class)klass;

@end
