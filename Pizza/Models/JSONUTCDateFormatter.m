//
//  JSONUTCDateFormatter.m
//  Nikita Titov
//
//  Created by Nikita Titov on 05/03/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "JSONUTCDateFormatter.h"

@implementation JSONUTCDateFormatter

+ (instancetype)sharedInstance {
    static JSONUTCDateFormatter *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[JSONUTCDateFormatter alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    self.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    self.dateFormat = @"yyyy-MM-dd HH:mm:ss";  // "2015-01-31 01:01:01"
    return self;
}

@end
