//
//  PZPizzaPlace.h
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <Mantle.h>

@interface PZPizzaPlace : MTLModel<MTLJSONSerializing, MTLManagedObjectSerializing>

@property (copy, nonatomic) NSString *id;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *address;
@property (assign, nonatomic) BOOL hasMenu;
@property (copy, nonatomic) NSURL *menuURL;

@end
