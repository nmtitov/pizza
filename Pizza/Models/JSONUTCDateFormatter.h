//
//  JSONUTCDateFormatter.h
//  Nikita Titov
//
//  Created by Nikita Titov on 05/03/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONUTCDateFormatter : NSDateFormatter

+ (instancetype)sharedInstance;

@end
