//
//  PZPizzaPlace.m
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZPizzaPlace.h"

@implementation PZPizzaPlace

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"id": @"id",
        @"name": @"name",
        @"phone": @"contact.formattedPhone",
        @"address": @"location.address",
        @"hasMenu": @"hasMenu",
        @"menuURL": @"menu.mobileUrl",
    };
}

+ (NSValueTransformer *)menuURLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSString *)managedObjectEntityName {
    return NSStringFromClass([self class]);
}

+ (NSDictionary *)managedObjectKeysByPropertyKey {
    return @{};
}

+ (NSSet *)propertyKeysForManagedObjectUniquing {
    return [NSSet setWithObject:@"id"];
}

@end
