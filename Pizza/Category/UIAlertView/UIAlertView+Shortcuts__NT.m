//
//  UIAlertView+Shortcuts.m
//  Nikita Titov
//
//  Created by Nikita Titov on 25/02/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "UIAlertView+Shortcuts__NT.h"

@implementation UIAlertView (Shortcuts__NT)

+ (void)showWithTitle:(NSString *)title message:(NSString *)message {
    [[[UIAlertView alloc] initWithTitle:title message:message] show];
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message {
    return [self initWithTitle:title
                       message:message
                      delegate:nil
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
}

@end
