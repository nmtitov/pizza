//
//  UIAlertView+Shortcuts.h
//  Nikita Titov
//
//  Created by Nikita Titov on 25/02/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Shortcuts__NT)

+ (void)showWithTitle:(NSString *)title message:(NSString *)message;
- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message;

@end
