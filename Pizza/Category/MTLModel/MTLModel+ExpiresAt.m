//
//  MTLModel+ExpiresAt.m
//  Nikita Titov
//
//  Created by Nikita Titov on 13/05/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "MTLModel+ExpiresAt.h"
#import "MTLModel+DateTransformer.h"

@implementation MTLModel (ExpiresAt)

+ (NSValueTransformer *)expiresAtJSONTransformer {
    return [self UTCDateTransformer];
}

- (BOOL)expired {
    // compare utc dates
    NSDate *date = [NSDate date];
    return [self.expiresAt compare:date] == NSOrderedAscending;
}

@end
