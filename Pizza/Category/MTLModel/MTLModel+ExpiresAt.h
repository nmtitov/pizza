//
//  MTLModel+ExpiresAt.h
//  Nikita Titov
//
//  Created by Nikita Titov on 13/05/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <Mantle/Mantle.h>

@protocol MTLModelExpiresAt <NSObject>

@optional

- (NSDate *)expiresAt;

@end

@interface MTLModel (ExpiresAt)<MTLModelExpiresAt>

+ (NSValueTransformer *)expiresAtJSONTransformer;

- (BOOL)expired;

@end
