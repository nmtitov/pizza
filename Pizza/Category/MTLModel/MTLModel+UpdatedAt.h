//
//  MTLModel+UpdatedAt.h
//  Nikita Titov
//
//  Created by Nikita Titov on 13/05/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MTLModel (UpdatedAt)

+ (NSValueTransformer *)updatedAtJSONTransformer;

@end
