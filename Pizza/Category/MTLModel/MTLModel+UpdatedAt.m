//
//  MTLModel+UpdatedAt.m
//  Nikita Titov
//
//  Created by Nikita Titov on 13/05/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "MTLModel+UpdatedAt.h"
#import "MTLModel+DateTransformer.h"

@implementation MTLModel (UpdatedAt)

+ (NSValueTransformer *)updatedAtJSONTransformer {
    return [self UTCDateTransformer];
}

@end
