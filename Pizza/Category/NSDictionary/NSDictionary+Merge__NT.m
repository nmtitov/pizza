//
//  NSDictionary+Merge__NT.m
//  Nikita Titov
//
//  Created by Nikita Titov on 13/05/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "NSDictionary+Merge__NT.h"

@implementation NSDictionary (Merge)

- (instancetype)merge__NT:(NSDictionary *)dict {
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:self];
    [result addEntriesFromDictionary:dict];
    return [result copy];
}

@end
