//
//  PZPizzaPlacesCell.m
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZPizzaPlacesCell.h"
#import "PZPizzaPlace.h"
#import <ReactiveCocoa.h>

@interface PZPizzaPlacesCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation PZPizzaPlacesCell

- (void)prepareForReuse {
    [super prepareForReuse];
    self.nameLabel.text = nil;
}

- (void)awakeFromNib {
    [self prepareForReuse];
    RAC(self.nameLabel, text) = RACObserve(self, item.name);
}

@end
