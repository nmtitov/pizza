//
//  PZPizzaPlacesCell.h
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PZPizzaPlace;
@interface PZPizzaPlacesCell : UITableViewCell

@property (strong, nonatomic) PZPizzaPlace *item;

@end
