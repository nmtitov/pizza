//
//  PZAppDelegate.h
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZError.h"
#import "PZHelper.h"
#import "PZLocationService.h"
#import <UIKit/UIKit.h>
#import <AFNetworking.h>
#import <AFNetworking-RACExtensions/AFHTTPSessionManager+RACSupport.h>
#import <CoreData/CoreData.h>

FOUNDATION_EXPORT NSString * const kCLIENT_ID;
FOUNDATION_EXPORT NSString * const kCLIENT_SECRET;
FOUNDATION_EXPORT NSString * const kVERSION;

FOUNDATION_EXPORT NSString * const kNOTIFICATION_SAVE;

@class PZAppDelegate;
PZAppDelegate * App();

@interface PZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AFHTTPSessionManager *http;
@property (strong, nonatomic) NSManagedObjectContext *moc;
@property (strong, nonatomic) PZLocationService *loc;

@end
