//
//  PZPizzaPlacesDetailViewController.m
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZPizzaPlacesDetailViewController.h"
#import "PZPizzaPlace.h"
#import "PZWebController.h"
#import <ReactiveCocoa.h>

static NSString * const kPZWebController = @"PZWebController";

@interface PZPizzaPlacesDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@end

@implementation PZPizzaPlacesDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    RAC(self.navigationItem, title) = RACObserve(self, item.name);
    RAC(self.addressLabel, text) = [RACObserve(self, item.address) map:^id(id value) {
        return value ?: NSLocalizedString(@"Not listed", @"");
    }];
    RAC(self.phoneLabel, text) = [RACObserve(self, item.phone) map:^id(id value) {
        return value ?: NSLocalizedString(@"Not Listed", @"");
    }];
    RAC(self.menuButton, hidden) = RACObserve(self, item.hasMenu).not;
}

#pragma mark - Actions

- (IBAction)actionOpenMenu:(id)sender {
    if (!self.item.menuURL) {
        return;
    }
    [self performSegueWithIdentifier:kPZWebController sender:self];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kPZWebController]) {
        PZWebController *controller = (PZWebController *)[segue.destinationViewController topViewController];
        controller.url = self.item.menuURL;
    }
}

@end
