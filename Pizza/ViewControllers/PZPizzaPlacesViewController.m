//
//  PZPizzaPlacesDetailViewController.m
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import "PZPizzaPlacesViewController.h"
#import "PZAppDelegate.h"
#import "PZMasterViewModel.h"
#import "PZPizzaPlacesDetailViewController.h"
#import "PZPizzaPlacesCell.h"
#import "FetchedResultsControllerDataSource.h"

NSString * const kPZDetailViewController = @"PZPizzaPlacesDetailViewController";

@interface PZPizzaPlacesViewController ()<BaseDataSourceDelegate>

@property (copy, nonatomic) NSString *reuseIdentifier;
@property (strong, nonatomic) UINib *cellNib;

@end

@implementation PZPizzaPlacesViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    self.reuseIdentifier = NSStringFromClass([PZPizzaPlacesCell class]);
    self.cellNib = [UINib nibWithNibName:self.reuseIdentifier bundle:nil];
}

- (void)viewDidLoad {
    NSParameterAssert(self.viewModel);
    
    [super viewDidLoad];
    [self setupDatasource];

    [self.tableView registerNib:self.cellNib forCellReuseIdentifier:self.reuseIdentifier];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.rac_command = self.viewModel.load;
    
    [self.viewModel.load execute:nil];
    
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:kNOTIFICATION_SAVE object:nil] subscribeNext:^(id x) {
        NSLog(@"Obtained new data");
    }];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:kPZDetailViewController sender:self];
}

#pragma mark - Core Data

static const int kBatchSize = 10;

- (void)setupDatasource {
    NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];

    NSFetchRequest *fetchRequest =
    fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[PZPizzaPlace managedObjectEntityName]];
    fetchRequest.sortDescriptors = @[
        sd,
    ];
    
    [fetchRequest setFetchBatchSize:kBatchSize];
    
    NSFetchedResultsController *fetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.moc sectionNameKeyPath:nil cacheName:nil];
    
    self.dataSource = [[FetchedResultsControllerDataSource alloc] initWithFetchedResultsController:fetchedResultsController cellIdentifier:self.reuseIdentifier
        itemMappingBlock:^id(id item) {
            NSError *error;
            PZPizzaPlace *mappedItem = [MTLManagedObjectAdapter modelOfClass:[PZPizzaPlace class] fromManagedObject:item error:&error];
            return mappedItem;
        }
        configureCellBlock:^(id<CellItem> cell, id item) {
            cell.item = item;
        }
        latestItemSortDescriptors:@[
            [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:NO],
        ]];
    self.dataSource.delegate = self;
}

#pragma mark - PBaseDataSourceDelegate

- (void)willShowCellForLastRow {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kPZDetailViewController]) {
        PZPizzaPlacesDetailViewController *controller = segue.destinationViewController;
        PZPizzaPlace *item = [self.dataSource itemAtIndexPath:self.tableView.indexPathForSelectedRow];
        controller.item = item;
    }
}

@end
