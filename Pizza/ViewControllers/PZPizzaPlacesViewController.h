//
//  PZPizzaPlacesDetailViewController.h
//  Pizza
//
//  Created by Nikita Titov on 20/06/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FetchedResultsTableViewController.h"

FOUNDATION_EXPORT NSString * const kPZDetailViewController;

@class PZMasterViewModel;
@interface PZPizzaPlacesViewController : FetchedResultsTableViewController

@property (strong, nonatomic) PZMasterViewModel *viewModel;
@property (strong, nonatomic) NSManagedObjectContext *moc;

@end
