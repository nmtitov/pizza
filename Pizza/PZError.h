//
//  PZError.h
//  Nikita Titov
//
//  Created by Nikita Titov on 14/05/15.
//  Copyright (c) 2015 Nikita Titov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PZError : NSError

+ (instancetype)requestError;
+ (instancetype)requestErrorWithMessage:(NSString *)message;
+ (instancetype)serverError;
+ (instancetype)serverErrorWithMessage:(NSString *)message;
+ (instancetype)parseError;
+ (instancetype)parseErrorWithMessage:(NSString *)message;
+ (instancetype)authenticationError;
+ (instancetype)authenticationErrorWithMessage:(NSString *)message;

@end

FOUNDATION_EXPORT NSString * const PZServerErrorDomain;
FOUNDATION_EXPORT NSInteger const PZServerErrorRequestError;
FOUNDATION_EXPORT NSInteger const PZServerErrorServerError;
FOUNDATION_EXPORT NSInteger const PZServerErrorParseError;
FOUNDATION_EXPORT NSInteger const PZServerErrorAuthenticationError;
